#include "util.h"

#include "lodepng.h"
#include <iostream>
#include <vector>

void printShaderInfoLogs(GLuint obj, GLuint shader) {
	int infologLength = 0;
	int charsWritten  = 0;
	char* infoLog;
	
	glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &infologLength);
	if (infologLength > 1) {
		infoLog = new char[infologLength];
		glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
		std::cerr << infoLog << std::endl;
		delete[] infoLog;
	}
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologLength);
	if (infologLength > 1) {
		infoLog = new char[infologLength];
		glGetShaderInfoLog(shader, infologLength, &charsWritten, infoLog);
		std::cerr << infoLog << std::endl;
		delete[] infoLog;
	}
}

void loadImage(char* filename, std::vector<unsigned char>& imgData, int& width, int& height) {
	int x, y, c;
	std::vector<unsigned char> buffer, image;
	LodePNG::loadFile(buffer, filename); //load the image file with given filename
	LodePNG::Decoder decoder;
	decoder.decode(image, buffer.size() ? &buffer[0] : 0, (unsigned)buffer.size()); //decode the png

	printf("loading %s, ", filename);
	//if there's an error, display it
	if (decoder.hasError()) std::cout << "error: " << decoder.getError() << std::endl;

	width = decoder.getWidth();
	height = decoder.getHeight();
	int numChannels = decoder.getChannels();

	printf("tex %d,%d, channels %d\n", width, height, numChannels);
	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			int i = 4 * (y * width + x);
			int s = imgData.size();
			for (c = 0; c < numChannels; c++) {
				imgData.push_back(image[i + c]);
			}
			for (c = numChannels; c < 4; c++) {
				imgData.push_back(255);
			}
			if (numChannels == 4) {
				float alpha = imgData[s + 3] / 255.f;
				for (c = 0; c < 3; c++) {
					imgData[s + c] *= alpha;
				}
			}
		}
	}
}
