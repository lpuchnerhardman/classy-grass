#pragma once
#include "main.h"
#include <stdlib.h>
#include <vector>
#include "glut.h"

class Grass: public Renderable {
public:
	Grass(int resolution); // 512
	virtual ~Grass();

	void setLight(bool turnOnLight);
	void update(vec2 charPos);
	void render(mat4& mvi, mat4& mvp);

private:
	void initShaders();
	void initTextures();

	std::vector<vec2> tufts;
	std::vector<GLuint> indices;

	int res;
	int numVertices;
	int numQuads;
	int numIndices;

	bool turnOnLight;

	GLuint shaderProgramID;
	GLuint vaoID;
	GLuint vboID;
	GLuint indexID;
};