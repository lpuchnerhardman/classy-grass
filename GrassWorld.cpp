#include "GrassWorld.h"

#include <iostream>
#include <algorithm>

void createTexture(GLenum tex, GLenum dataType, void* data) {
	GLuint id;
	glActiveTexture(tex);
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, dataType, RES, RES, 0, dataType, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
}
GrassWorld::GrassWorld(): ground(RES), grass(RES), time(0), perlin(0.5, 4), charPos(0, 20, 14), roteX(0), roteY(0) {
	for (int i = 0; i < KEY_COUNT; i++) {
		keysDown[i] = false;
	}

	keyLightPress = true;

	GLuint heightmapTextureID;
	for (int y = 0; y < RES; y++) {
		for (int x = 0; x < RES; x++) {
			hmArr[y][x].w = (perlin.at(x * 4. / RES, y * 4. / RES) + 1) / 2;
		}
	}
	for (int y = 0; y < RES; y++) {
		for (int x = 0; x < RES; x++) {
			if (x == 0 || y == 0 || x == RES - 1 || y == RES - 1) {
				hmArr[y][x] = vec4(0.5, 1.0, 0.5, hmArr[y][x].w);
			} else {
				vec3 tl(x - 1, hmArr[y - 1][x - 1].w, y - 1);
				vec3 tr(x + 1, hmArr[y - 1][x + 1].w, y - 1);
				vec3 bl(x - 1, hmArr[y + 1][x - 1].w, y + 1);
				vec3 br(x + 1, hmArr[y + 1][x + 1].w, y + 1);
				vec3 norm = (cross(tr - br, bl - br)).normalize() / 2 + 0.5;
				hmArr[y][x] = vec4(norm.x, norm.y, norm.z, hmArr[y][x].w);
			}
		}
	}
	glActiveTexture(HEIGHT_TEX_GL);
	glGenTextures(1, &heightmapTextureID);
	glBindTexture(GL_TEXTURE_2D, heightmapTextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, RES, RES, 0, GL_RGBA, GL_FLOAT, hmArr);
	glGenerateMipmap(GL_TEXTURE_2D);

	GLuint colormapTextureID;
	for (int y = 0; y < RES; y++) {
		for (int x = 0; x < RES; x++) {
			cmArr[y][x] = (perlin.at(-x * 30. / RES, -y * 30. / RES) + 1) * 255;
		}
	}
	glActiveTexture(COLOR_TEX_GL);
	glGenTextures(1, &colormapTextureID);
	glBindTexture(GL_TEXTURE_2D, colormapTextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, RES, RES, 0, GL_RED, GL_UNSIGNED_BYTE, cmArr);
	glGenerateMipmap(GL_TEXTURE_2D);

	std::cout << "Press 'L' to toggle the lighting on and off." << std::endl;
}

// If the size of the window changed, call this to update the GL matrices
void GrassWorld::resize(int w, int h) {
	
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0) h = 1;
	
	float aspect = ((float)w) / h;

	// Calculate the projection matrix    
	float fovy = 45.0f;
	float near = 0.01f;
	float far = 1000.0f;
	projectionMatrix = Perspective(fovy, aspect, near, far);

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
}

void GrassWorld::update() {
	float currentTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	float timePassed  = currentTime - time;
	time = currentTime;
	float movement = timePassed * 30;
	if (keysDown[UP]) {
		charPos.x += movement * sin(roteY);
		charPos.z += movement * cos(roteY);
	}
	if (keysDown[DOWN]) {
		charPos.x -= movement * sin(roteY);
		charPos.z -= movement * cos(roteY);
	}
	if (keysDown[LEFT]) {
		charPos.x +=  movement * cos(roteY);
		charPos.z += -movement * sin(roteY);
	}
	if (keysDown[RIGHT]) {
		charPos.x -=  movement * cos(roteY);
		charPos.z -= -movement * sin(roteY);
	}

	if (keysDown[FOCUS]) {
		roteX += -(mouseCurrentPos.y - mouseMove.y) / (float)glutGet(GLUT_WINDOW_HEIGHT);
		roteX = std::min(roteX, (PI / 2) *  0.99f);
		roteX = std::max(roteX, (PI / 2) * -0.99f);
		roteY +=  (mouseCurrentPos.x - mouseMove.x) / (float)glutGet(GLUT_WINDOW_WIDTH);
		mouseCurrentPos = mouseMove;
	}

	if (keyLightPress) {
		turnOnLight = true;
	}

	render();
}
void GrassWorld::render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	vec2 scaledPos = (vec2(charPos.x, charPos.z) / 50 + 1) / 2;
	charPos.y = (perlin.at((charPos.x / 50 + 1) / 2 * 4, (charPos.z / 50 + 1) / 2 * 4) + 1.2) * 25;

	mat4 rotDirX, rotDirY;
	rotDirX.rotX(roteX / PI * 180);
	rotDirY.rotY(roteY / PI * 180);
	vec4 v = rotDirY * rotDirX * vec4(0, 0, 1, 1);

	vec3 up(0, 1, 0);
	mat4 view = LookAt(charPos, charPos + vec3(v.x, v.y, v.z), up); // Defines View Matrix

	mat4 mvp = projectionMatrix * view;
	mat4 mvi = view.inverse();

	skybox.setCharPos(charPos);
	skybox.render(mvi, mvp);

	ground.render(mvi, mvp);
	
	grass.setLight(keyLightPress);
	grass.update(vec2(charPos.x, charPos.z));
	grass.render(mvi, mvp);

	glutSwapBuffers();
}

void GrassWorld::normalKeyDown(unsigned char key, int x, int y) {
	switch (key) {
		case 'l': keyLightPress = !keyLightPress; break;
		case 27: exit(0); break;
		default: break;
	}
}
void GrassWorld::normalKeyUp(unsigned char key, int x, int y) { }
void GrassWorld::specialKeyDown(int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_LEFT:  keysDown[LEFT]  = true; break;
		case GLUT_KEY_RIGHT: keysDown[RIGHT] = true; break;
		case GLUT_KEY_UP:    keysDown[UP]    = true; break;
		case GLUT_KEY_DOWN:  keysDown[DOWN]  = true; break;
		default: break;
	}
}
void GrassWorld::specialKeyUp(int key, int x, int y) {
	switch (key) {
		case GLUT_KEY_LEFT:  keysDown[LEFT]  = false; break;
		case GLUT_KEY_RIGHT: keysDown[RIGHT] = false; break;
		case GLUT_KEY_UP:    keysDown[UP]    = false; break;
		case GLUT_KEY_DOWN:  keysDown[DOWN]  = false; break;
	}
}

void GrassWorld::processMouse(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		keysDown[MOUSE] = (state == GLUT_DOWN);
		if (!keysDown[MOUSE]) {
			keysDown[FOCUS] = !keysDown[FOCUS];
			if (keysDown[FOCUS]) {
				glutSetCursor(GLUT_CURSOR_NONE);
			} else {
				glutSetCursor(GLUT_CURSOR_INHERIT);
			}
		}
	}
}

void GrassWorld::processMouseMotion(int x, int y) {
	mouseMove = vec2(x, y);
	if (keysDown[FOCUS]) {
		int wid = glutGet(GLUT_WINDOW_WIDTH);
		int hei = glutGet(GLUT_WINDOW_HEIGHT);
		if (x < wid / 4 || x >= wid / 4 * 3 || y < hei / 4 || y >= hei / 4 * 3) {
			mouseCurrentPos = vec2(wid / 2, hei / 2);
			glutWarpPointer(wid / 2, hei / 2);
		}
	} else {
		mouseCurrentPos = vec2(x, y);
	}
}
