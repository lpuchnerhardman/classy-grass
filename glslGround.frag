
#version 150

uniform vec3 vCameraPos;
uniform sampler2D GroundTexture;
uniform sampler2D HeightmapTexture;

in vec2 texcoord;
in vec3 position;
in vec3 normal;

void main() {
	vec4 color = texture(GroundTexture, texcoord);

	vec3 lightPos = vec3(100, 100, -100);
	vec3 L = normalize(lightPos - position);
	vec3 E = normalize(vCameraPos - position);
	vec3 R = normalize(-reflect(L, normal));
	float phong = 0.5 * max(0, pow(dot(R, E), 5)) + max(0, dot(normal, L)) + 0.2;
	color.rgb = color.rgb * phong * 0.9;

    gl_FragColor = color;
}