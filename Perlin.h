#pragma once

class Perlin {
public:
	Perlin(float persistance, unsigned int numOctaves);
	float at(float x, float y);

private:
	float interpolatedNoise(float x, float y);
	float smoothNoise(int x, int y);
	float noise(int x, int y);
	float interpolate(float v1, float v2, float d);
	float persistance;
	unsigned int numOctaves;
};

