#pragma once
#include "main.h"
#include <stdlib.h>
#include <vector>
#include "matrix.h"
#include "glut.h"

class Skybox: public Renderable {
public:
	Skybox(); 
	virtual ~Skybox();

	void setCharPos(vec3 charPos);
	void render(mat4& mvi, mat4& mvp);

private:
	void initShaders();
	void initTextures();
	void indexFace(GLuint* indices, int a, int b, int c, int d);
	
	std::vector<vec3> vertices;

	int numVertices;
	int numTriangles;
	int numIndices;

	vec3 charPos;

	GLuint shaderProgramID;
	GLuint vaoID;
	GLuint vboID;
	GLuint indexID;
};