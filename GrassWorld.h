#pragma once

#include "Perlin.h"
#include "Ground.h"
#include "Grass.h"
#include "Skybox.h"
#include "matrix.h"

static const int RES = 64;

class GrassWorld {
public:
	GrassWorld();
	void resize(int w, int h);
	void update();
	void render();
	void normalKeyDown(unsigned char key, int x, int y);
	void normalKeyUp(unsigned char key, int x, int y);
	void specialKeyDown(int key, int x, int y);
	void specialKeyUp(int key, int x, int y);
	void processMouse(int button, int state, int x, int y);
	void processMouseMotion(int x, int y);

private:
	void render(Renderable& toRender, mat4& view, mat4& mvp);
	Ground ground;
	Grass  grass;
	Skybox skybox;

	float time;
	Perlin perlin;

	vec3 charPos;
	float roteX, roteY;
	vec2 mouseMove, mouseCurrentPos;
	mat4 projectionMatrix;

	vec4    hmArr[RES][RES];
	GLubyte cmArr[RES][RES];

	enum Keys { MOUSE, FOCUS, LEFT, RIGHT, UP, DOWN, KEY_COUNT };
	bool keysDown[KEY_COUNT];

	bool keyLightPress, turnOnLight;
};
