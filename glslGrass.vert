
#version 150

in vec3 vPosition;
in vec3 vTexCoord;

uniform mat4 ModelViewProjectionMatrix;
uniform vec3 vCameraPos;
uniform sampler2D HeightmapTexture;
uniform float time;

/* View vector */
out vec3 texcoord;
out vec3 position;
out vec3 normal;

void main() {
	texcoord = vTexCoord;
	vec3 pos = vPosition;

	vec4 h = texture(HeightmapTexture, ((vPosition.xz / 50.0) + 1.0) / 2.0);
	normal = h.xyz * 2 - 1;
	if (pos.y > 0.1) {
		float off = time + vPosition.x / 8;
		float hchange = sin(off * 3) + sin(off * 4) + sin(off / 5) + sin(off / 7);
		float vchange = cos(off * 3) + cos(off * 4) + cos(off / 5) + cos(off / 7);
		pos.x += hchange / 6;
		pos.y += vchange / 8;
		pos.z += hchange / 6;
		normal = normalize(normal + vec3(hchange / 6, vchange / 8, hchange / 6));
	}
	pos.y += h.w * 50;
	position = pos;

	gl_Position = ModelViewProjectionMatrix * vec4(pos.xyz, 1.0);
}