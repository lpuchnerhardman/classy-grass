#include "GrassWorld.h"

#include <memory>

std::unique_ptr<GrassWorld> world;

void reshapeFunc(int w, int h) {
	world->resize(w, h);
}
void render() {
	world->update();
}
void keyboardFunc(  unsigned char key, int x, int y) { world->normalKeyDown( key, x, y); }
void keyboardUpFunc(unsigned char key, int x, int y) { world->normalKeyUp(   key, x, y); }
void specialFunc(             int key, int x, int y) { world->specialKeyDown(key, x, y); }
void specialUpFunc(           int key, int x, int y) { world->specialKeyUp(  key, x, y); }
void mouseFunc(int button, int state, int x, int y) {
	world->processMouse(button, state, x, y);
}
void motionFunc(int x, int y) {
	world->processMouseMotion(x, y);
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1000, 800);
	glutCreateWindow("GLSL Water");
	
	GLenum err = glewInit();

	// Test for OpenGL 3
	if (GLEW_VERSION_3_0) {
		printf("GL version 3 supported \n");
	}
	
	world.reset(new GrassWorld());

	glutDisplayFunc(render);
	glutIdleFunc(render);
	glutReshapeFunc(reshapeFunc);
	glutKeyboardFunc(keyboardFunc);
	glutKeyboardUpFunc(keyboardUpFunc);
	glutSpecialFunc(specialFunc);
	glutSpecialUpFunc(specialUpFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(motionFunc);
	glutPassiveMotionFunc(motionFunc);
	glutIgnoreKeyRepeat(true); 

	glClearColor(0.3, 0.4, 0.6, 1.0);

	glutMainLoop();

	return 0;
}