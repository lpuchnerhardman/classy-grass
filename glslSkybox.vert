
#version 150

/* Per-vertex input from application */
in vec3 vPosition;

uniform vec3 charPos;
uniform mat4 ModelViewProjectionMatrix;

out vec3 texcoord;

void main() {

	texcoord = vPosition;

	gl_Position = ModelViewProjectionMatrix * vec4(vPosition + charPos, 1.0);
}