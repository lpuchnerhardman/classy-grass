#pragma once
#include <GL/glew.h>
#include "matrix.h"

const int GROUND_TEX_GL = GL_TEXTURE0;
const int GROUND_TEX_I  = 0;
const int  GRASS_TEX_GL = GL_TEXTURE1;
const int  GRASS_TEX_I  = 1;
const int HEIGHT_TEX_GL = GL_TEXTURE2;
const int HEIGHT_TEX_I  = 2;
const int  COLOR_TEX_GL = GL_TEXTURE3;
const int  COLOR_TEX_I  = 3;
const int SKYBOX_TEX_GL = GL_TEXTURE4;
const int SKYBOX_TEX_I  = 4;
const float PI = 3.1415926f;

class Renderable {
public:
	virtual void update(vec3 charPos) { }
	virtual void render(mat4& mvi, mat4& mvp) = 0;
};