#include "Ground.h"

#include "util.h"
#include <iostream>
#include <vector>

Ground::Ground(int res): res(res) {
	numVertices = (res + 1) * (res + 1);
	numQuads    = res * res;
	numIndices  = 4 * numQuads;
	float delta = 2.0f / res;

	//Generate grid positions
	std::vector<vec3>  vertices(numVertices);
	std::vector<vec2> texcoords(numVertices);
	std::vector<GLuint> indices(numIndices);

	for (int y = 0; y <= res; y++) {
		for (int x = 0; x <= res; x++) {
			int vertexPosition = y * (res + 1) + x;
			vertices[vertexPosition]  = vec3(x * delta - 1.0, 0, y * delta - 1.0) * 50.f;
			texcoords[vertexPosition] = vec2(x, y) * delta;
		}
	}

	// Generate indices into vertex list
	for (int y = 0; y < res; y++) {
		for (int x = 0; x < res; x++) {
			int indexPosition = 4 * (y * res + x);
			indices[indexPosition + 0] =  y      * (res + 1) + x;
			indices[indexPosition + 1] = (y + 1) * (res + 1) + x;
			indices[indexPosition + 2] = (y + 1) * (res + 1) + x + 1;
			indices[indexPosition + 3] =  y      * (res + 1) + x + 1;
		}
	}

    // Create a vertex array object
    glGenVertexArrays(1, &vaoID);
    glBindVertexArray(vaoID);

	int vlen = numVertices * sizeof(vertices[0]);
	int tlen = numVertices * sizeof(texcoords[0]);
	int ilen = numIndices  * sizeof(indices[0]);

    // Create and initialize a buffer object
    glGenBuffers(1, &vboID);
    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, vlen + tlen, NULL, GL_STATIC_DRAW);

	// Set the buffer pointers
	glBufferSubData(GL_ARRAY_BUFFER,    0, vlen, vertices.data());
	glBufferSubData(GL_ARRAY_BUFFER, vlen, tlen, texcoords.data());

	// Bind the index buffer
	glGenBuffers(1, &indexID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ilen, indices.data(), GL_STATIC_DRAW);

	// Initalize shaders
	initShaders();

	// Initialize the vertex position attribute from the vertex shader
	GLuint pos = glGetAttribLocation(shaderProgramID, "vPosition");
	glEnableVertexAttribArray(pos);
	glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint tex = glGetAttribLocation(shaderProgramID, "vTexCoord");
	glEnableVertexAttribArray(tex);
	glVertexAttribPointer(tex, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)vlen);

	initTextures();

	glUniform1i(glGetUniformLocation(shaderProgramID, "GroundTexture"),    GROUND_TEX_I);
	glUniform1i(glGetUniformLocation(shaderProgramID, "HeightmapTexture"), HEIGHT_TEX_I);
}

void Ground::initShaders() {
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint pixelShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	
	std::string vs = textFileRead("../glslGround.vert");
	std::string fs = textFileRead("../glslGround.frag");
	
	const char* c_str;
	glShaderSource(vertexShaderID, 1, &(c_str = vs.c_str()), nullptr);
	glShaderSource(pixelShaderID,  1, &(c_str = fs.c_str()), nullptr);
	glCompileShader(vertexShaderID);
	glCompileShader(pixelShaderID);
	
	shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, pixelShaderID);
	printShaderInfoLogs(shaderProgramID, pixelShaderID);
	glAttachShader(shaderProgramID, vertexShaderID);
	printShaderInfoLogs(shaderProgramID, vertexShaderID);
	
	glLinkProgram(shaderProgramID);
	glUseProgram(shaderProgramID);
}

void Ground::initTextures() {
	int width, height;
	std::vector<unsigned char> imgData;
	loadImage("../ground2.png", imgData, width, height);

	GLuint groundTextureID;
	glActiveTexture(GROUND_TEX_GL);
	glGenTextures(1, &groundTextureID);
	glBindTexture(GL_TEXTURE_2D, groundTextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA , GL_UNSIGNED_BYTE, imgData.data());
	glGenerateMipmap(GL_TEXTURE_2D);
}

void Ground::render(mat4& mvi, mat4& mvp) {
	glBindVertexArray(vaoID);
	glUseProgram(shaderProgramID);
   	
	GLuint mvpID = glGetUniformLocation(shaderProgramID, "ModelViewProjectionMatrix");
	glUniformMatrix4fv(mvpID, 1, GL_TRUE, (GLfloat*)mvp.getFloatArray());

	GLuint camPosID = glGetUniformLocation(shaderProgramID, "vCameraPos");
	glUniform3f(camPosID, mvi(0, 3), mvi(1, 3), mvi(2, 3));

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);	
	glDrawElements(GL_QUADS, numIndices, GL_UNSIGNED_INT, 0);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}

Ground::~Ground() {
	glDeleteBuffers(1, &vboID);
	glDeleteBuffers(1, &indexID);
	glDeleteVertexArrays(1, &vaoID);
	glDeleteProgram(shaderProgramID);
}