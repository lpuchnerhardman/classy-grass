
#version 150

uniform int lightOn;
uniform vec3 vCameraPos;
uniform sampler2DArray GrassTexture;
uniform sampler2D ColormapTexture;
uniform sampler2D HeightmapTexture;

in vec3 texcoord;
in vec3 cpos;
in vec3 position;
in vec3 normal;

void main() {
	float colorChange = texture(ColormapTexture, ((position.xz / 50.0) + 1.0) / 2.0).r;
	vec4 color = texture(GrassTexture, texcoord);
	color.r = clamp(color.r + (colorChange - 0.5) / 4.0, 0.0, 1.0) * color.a;

	if (lightOn == 1) {
		vec3 lightPos = vec3(100, 100, -100);
		vec3 L = normalize(lightPos - position);
		vec3 E = normalize(vCameraPos - position);
		vec3 R = normalize(-reflect(L, normal));
		float phong = 0.5 * max(0, pow(dot(R, E), 5)) + max(0, dot(normal, L)) + 0.2;
		color.rgb = color.rgb * phong * 0.9;
	}

	gl_FragColor = color;
}