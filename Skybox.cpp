#include "Skybox.h"

#include "util.h"
#include <algorithm>
#include <iostream>
#include <map>

Skybox::Skybox() {

	numVertices  = 8;
	numTriangles = 12;
	numIndices   = 3 * numTriangles;

	//Generate grid positions
	vertices.resize(numVertices);
	//std::vector<vec2> texcoords(numVertices);
	std::vector<GLuint> indices(numIndices);

	/*Init vertices*/
	/*for (int i = 0; i < numVertices; i++){
		vertices[i] = vec3(0, 0, 0);
	}*/

	int faceSize = 100;
	int t = faceSize/2;

	// Front face
	vertices[0] = vec3(- t, - t,  t); // bl
	vertices[1] = vec3( t, -t,  t); // br
	vertices[2] = vec3( -t,  t, t); // tl
	vertices[3] = vec3( t,  t, t); // tr

	// Back face
	vertices[4] = vec3(- t, - t,  -t); // bl
	vertices[5] = vec3( t, -t,  -t); // br
	vertices[6] = vec3( -t,  t, -t); // tl
	vertices[7] = vec3( t,  t, -t); // tr

	/*Init texcoord*/

	// Generate indices into vertex list for each square of the cube

	// Front face
	indexFace(&indices[0], 0, 1, 2, 3);
	// Back face
	indexFace(&indices[6], 4, 5, 6, 7);
	// Top face
	indexFace(&indices[12], 2, 3, 6, 7);
	// Bottom face
	indexFace(&indices[18], 0, 1, 4, 5);
	// Right face
	indexFace(&indices[24], 1, 5, 3, 7);
	// Left face
	indexFace(&indices[30], 0, 4, 2, 6);

	// Create a vertex array object
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	int vlen = numVertices * sizeof(vertices[0]);
	//int tlen = numVertices * sizeof(texcoords[0]);
	int ilen = 3 * numTriangles * sizeof(indices[0]);

	// Create and initialize a buffer object
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	//glBufferData(GL_ARRAY_BUFFER, vlen + tlen, nullptr, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, vlen, nullptr, GL_STATIC_DRAW);

	// Set the buffer pointers
	glBufferSubData(GL_ARRAY_BUFFER,    0, vlen, vertices.data());
	//glBufferSubData(GL_ARRAY_BUFFER, vlen, tlen, texcoords.data());

	// Bind the index buffer
	glGenBuffers( 1, &indexID );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ilen, indices.data(), GL_STATIC_DRAW);

	// Initalize shaders
	initShaders();

	// Initialize the vertex position attribute from the vertex shader
	GLuint pos = glGetAttribLocation(shaderProgramID, "vPosition");
	glEnableVertexAttribArray(pos);
	glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//GLuint tex = glGetAttribLocation(shaderProgramID, "vTexCoord");
	//glEnableVertexAttribArray(tex);
	//glVertexAttribPointer(tex, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)vlen);

	initTextures();

	glUniform1i(glGetUniformLocation(shaderProgramID, "SkyboxTexture"),  SKYBOX_TEX_I);
}

void Skybox::initShaders() {
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint pixelShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	
	std::string vs = textFileRead("../glslSkybox.vert");
	std::string fs = textFileRead("../glslSkybox.frag");
	
	const char* c_str;
	glShaderSource(vertexShaderID, 1, &(c_str = vs.c_str()), nullptr);
	glShaderSource(pixelShaderID,  1, &(c_str = fs.c_str()), nullptr);
	glCompileShader(vertexShaderID);
	glCompileShader(pixelShaderID);
	
	shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, pixelShaderID);
	printShaderInfoLogs(shaderProgramID, pixelShaderID);
	glAttachShader(shaderProgramID, vertexShaderID);
	printShaderInfoLogs(shaderProgramID, vertexShaderID);
	
	glLinkProgram(shaderProgramID);
	glUseProgram(shaderProgramID);
}

void Skybox::initTextures() {

/* Skybox */
	int width, height;
	std::vector<unsigned char> imgData;

	GLuint SkyboxTextureID;

	glActiveTexture(SKYBOX_TEX_GL);
	glGenTextures(1, &SkyboxTextureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, SkyboxTextureID);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	loadImage((char*)"../posx.png", imgData, width, height);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
	imgData.clear();
	loadImage((char*)"../negx.png", imgData, width, height);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
	imgData.clear();
	loadImage((char*)"../posy.png", imgData, width, height);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
	imgData.clear();
	loadImage((char*)"../negy.png", imgData, width, height);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
	imgData.clear();
	loadImage((char*)"../posz.png", imgData, width, height);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
	imgData.clear();
	loadImage((char*)"../negz.png", imgData, width, height);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
}

void Skybox::indexFace(GLuint* indices, int a, int b, int c, int d){

	// tri0
	indices[0] = a;
	indices[1] = b; 
	indices[2] = c;

	//tri1
	indices[3] = b;
	indices[4] = d;
	indices[5] = c;

}

void Skybox::setCharPos(vec3 charPos) {
	this->charPos = charPos;
}

void Skybox::render(mat4& mvi, mat4& mvp) {
	glBindVertexArray(vaoID);
	glUseProgram(shaderProgramID);

	GLuint mvpID = glGetUniformLocation(shaderProgramID, "ModelViewProjectionMatrix");
	glUniformMatrix4fv(mvpID, 1, GL_TRUE, (GLfloat*)mvp.getFloatArray());

	GLuint camPosID = glGetUniformLocation(shaderProgramID, "vCameraPos");
	glUniform3f(camPosID, mvi(0, 3), mvi(1, 3), mvi(2, 3));

	float time = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	glUniform1f(glGetUniformLocation(shaderProgramID, "time"), time);

	glUniform3f(glGetUniformLocation(shaderProgramID, "charPos"), charPos.x, charPos.y, charPos.z);	

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
}

Skybox::~Skybox() {
	glDeleteBuffers(1, &vboID);
	glDeleteBuffers(1, &indexID);
	glDeleteVertexArrays(1, &vaoID);
	glDeleteProgram(shaderProgramID);
}