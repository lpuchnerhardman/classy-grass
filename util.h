#pragma once

#include <fstream>
#include <GL/glew.h>
#include <vector>
#include "glut.h"

inline std::string textFileRead(const std::string& filename) {
	std::ifstream t(filename);
	return std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
}
void printShaderInfoLogs(GLuint obj, GLuint shader);
void loadImage(char* filename, std::vector<unsigned char>& imgData, int& width, int& height);
