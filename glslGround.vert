
#version 150

in vec3 vPosition;
in vec2 vTexCoord;

uniform mat4 ModelViewProjectionMatrix;
uniform vec3 vCameraPos;
uniform sampler2D GroundTexture;
uniform sampler2D HeightmapTexture;

/* View vector */
out vec2 texcoord;
out vec3 position;
out vec3 normal;

void main() {
	texcoord = vTexCoord;

	vec4 h = texture(HeightmapTexture, ((vPosition.xz / 50.0) + 1.0) / 2.0);
	normal = h.xyz * 2 - 1;
	vec3 pos = vPosition;
	pos.y = h.a * 50;
	position = pos;

	gl_Position = ModelViewProjectionMatrix * vec4(pos.xyz, 1.0);
}