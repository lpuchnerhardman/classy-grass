#include "Grass.h"

#include "util.h"
#include "Perlin.h"
#include <algorithm>
#include <iostream>

Grass::Grass(int res): res(res) {

	numVertices = 12 * res * res;
	numQuads    = 3 * res * res;
	numIndices  = 4 * numQuads;

	//Generate grid positions
	const float scale = 50.0f;
	const float delta = 2.0f / res;

	std::vector<vec3>  vertices(numVertices);
	std::vector<vec3> texcoords(numVertices);
	tufts.resize(res * res);
	indices.resize(numIndices);

	for (int y = 0; y < res; y++) {
		for (int x = 0; x < res; x++) {
			float x0 = x + (rand() / (float)RAND_MAX - 0.5) / 2;
			float y0 = y + (rand() / (float)RAND_MAX - 0.5) / 2;
			tufts[y * res + x] = (vec2(x0, y0) * delta - 1.0) * scale;
			float height = delta * scale * 2 + (rand() / (float)RAND_MAX);
			float rote = rand() / (float)RAND_MAX * PI * 2;
			
			int type = rand() % 30;
			if (!type) type = 3;
			else type = type < 5;

			for (int r = 0; r < 3; r++) {
				int vertPos = (y * res + x) * 12 + r * 4;
				vec2 off = vec2(cos(rote + r * 2 * PI / 3), sin(rote + r * 2 * PI / 3));
				vec2 end1 = (vec2(x0 + off.x, y0 + off.y) * delta - 1.0) * scale;
				vec2 end2 = (vec2(x0 - off.x, y0 - off.y) * delta - 1.0) * scale;
				vertices[vertPos    ] = vec3(end1.x,      0, end1.y);
				vertices[vertPos + 1] = vec3(end2.x,      0, end2.y);
				vertices[vertPos + 2] = vec3(end2.x, height, end2.y);
				vertices[vertPos + 3] = vec3(end1.x, height, end1.y);
				texcoords[vertPos    ] = vec3(0.0, 1.0, type);
				texcoords[vertPos + 1] = vec3(1.0, 1.0, type);
				texcoords[vertPos + 2] = vec3(1.0, 0.0, type);
				texcoords[vertPos + 3] = vec3(0.0, 0.0, type);
			}
		}
	}

	// Generate indices into vertex list
	for (int i = 0; i < numIndices; i++) {
		indices[i] = 0;
	}

	// Create a vertex array object
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	int vlen = numVertices * sizeof(vertices[0]);
	int tlen = numVertices * sizeof(texcoords[0]);
	int ilen = numIndices  * sizeof(indices[0]);

	// Create and initialize a buffer object
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, vlen + tlen, nullptr, GL_STATIC_DRAW);

	// Set the buffer pointers
	glBufferSubData(GL_ARRAY_BUFFER,    0, vlen, vertices.data());
	glBufferSubData(GL_ARRAY_BUFFER, vlen, tlen, texcoords.data());

	// Bind the index buffer
	glGenBuffers( 1, &indexID );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ilen, indices.data(), GL_STATIC_DRAW);

	// Initalize shaders
	initShaders();

	// Initialize the vertex position attribute from the vertex shader
	GLuint pos = glGetAttribLocation(shaderProgramID, "vPosition");
	glEnableVertexAttribArray(pos);
	glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint tex = glGetAttribLocation(shaderProgramID, "vTexCoord");
	glEnableVertexAttribArray(tex);
	glVertexAttribPointer(tex, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)vlen);

	initTextures();

	glUniform1i(glGetUniformLocation(shaderProgramID, "GrassTexture"),      GRASS_TEX_I);
	glUniform1i(glGetUniformLocation(shaderProgramID, "HeightmapTexture"), HEIGHT_TEX_I);
	glUniform1i(glGetUniformLocation(shaderProgramID,  "ColormapTexture"),  COLOR_TEX_I);
}

void Grass::initShaders() {
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint pixelShaderID = glCreateShader(GL_FRAGMENT_SHADER);
	
	std::string vs = textFileRead("../glslGrass.vert");
	std::string fs = textFileRead("../glslGrass.frag");
	
	const char* c_str;
	glShaderSource(vertexShaderID, 1, &(c_str = vs.c_str()), nullptr);
	glShaderSource(pixelShaderID,  1, &(c_str = fs.c_str()), nullptr);
	glCompileShader(vertexShaderID);
	glCompileShader(pixelShaderID);
	
	shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, pixelShaderID);
	printShaderInfoLogs(shaderProgramID, pixelShaderID);
	glAttachShader(shaderProgramID, vertexShaderID);
	printShaderInfoLogs(shaderProgramID, vertexShaderID);
	
	glLinkProgram(shaderProgramID);
	glUseProgram(shaderProgramID);
}

void Grass::initTextures() {
	int width, height;
	std::vector<unsigned char> imgData;
	loadImage("../grass1.png", imgData, width, height);
	loadImage("../grass2.png", imgData, width, height);
	loadImage("../grass3.png", imgData, width, height);
	loadImage("../grass4.png", imgData, width, height);

	GLuint grassTextureID;
	glActiveTexture(GRASS_TEX_GL);
	glGenTextures(1, &grassTextureID);
	glBindTexture(GL_TEXTURE_2D_ARRAY, grassTextureID);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, width, height, 4, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgData.data());
	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
}

void Grass::update(vec2 charPos) {
	std::vector<std::pair<float, int>> sqrDistances(tufts.size());
	for (size_t i = 0; i < tufts.size(); i++) {
		sqrDistances[i] = std::make_pair((tufts[i] - charPos).length2(), i);
	}
	std::sort(sqrDistances.begin(), sqrDistances.end());

	// Generate indices into vertex list
	for (size_t i = 0; i < sqrDistances.size(); i++) {
		int j = sqrDistances.size() - i - 1;
		for (int r = 0; r < 3; r++) {
			int indexPosition  = i * 12 + r * 4;
			int vertexPosition = sqrDistances[j].second * 12 + r * 4;
			for (int k = 0; k < 4; k++) {
				indices[indexPosition + k] = vertexPosition + k;
			}
		}
	}

	int ilen = numIndices * sizeof(indices[0]);
	glBindVertexArray(vaoID);
	glGenBuffers(1, &indexID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ilen, indices.data(), GL_STATIC_DRAW);
}

void Grass::setLight(bool turnOnLight) {
	this->turnOnLight = turnOnLight;
}

void Grass::render(mat4& mvi, mat4& mvp) {
	glBindVertexArray(vaoID);
	glUseProgram(shaderProgramID);

	GLuint mvpID = glGetUniformLocation(shaderProgramID, "ModelViewProjectionMatrix");
	glUniformMatrix4fv(mvpID, 1, GL_TRUE, (GLfloat*)mvp.getFloatArray());

	GLuint camPosID = glGetUniformLocation(shaderProgramID, "vCameraPos");
	glUniform3f(camPosID, mvi(0, 3), mvi(1, 3), mvi(2, 3));

	float time = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	glUniform1f(glGetUniformLocation(shaderProgramID, "time"), time);

	glUniform1i(glGetUniformLocation(shaderProgramID, "lightOn"), turnOnLight);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_QUADS, numIndices, GL_UNSIGNED_INT, 0);
	glDisable(GL_BLEND);
}

Grass::~Grass() {
	glDeleteBuffers(1, &vboID);
	glDeleteBuffers(1, &indexID);
	glDeleteVertexArrays(1, &vaoID);
	glDeleteProgram(shaderProgramID);
}