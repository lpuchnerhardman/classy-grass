#pragma once
#include "main.h"
#include <stdlib.h>
#include "glut.h"

class Ground: public Renderable {
public:
	Ground(int resolution); // 512
	virtual ~Ground();

	void render(mat4& mvi, mat4& mvp);

private:
	void initShaders();
	void initTextures();

	int res;
	int numVertices;
	int numQuads;
	int numIndices;

	GLuint shaderProgramID;
	GLuint vaoID;
	GLuint vboID;
	GLuint indexID;
};