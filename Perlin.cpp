#include "Perlin.h"
#include <cmath>

Perlin::Perlin(float persistance, unsigned int numOctaves): persistance(persistance), numOctaves(numOctaves - 1) { }

float Perlin::at(float x, float y) {
	float value = 0;
	for (int i = 0; i < (unsigned int)numOctaves; i++) {
		float frequency = pow(2.f, i);
		float amplitude = pow(persistance, i);
		value += interpolatedNoise(x * frequency, y * frequency) * amplitude;
	}
	return value;
}

float Perlin::interpolatedNoise(float x, float y) {
	int x0 = floor(x);
	int y0 = floor(y);
	float xd = x - x0;
	float yd = y - y0;

	float v1 = smoothNoise(x0,     y0);
	float v2 = smoothNoise(x0 + 1, y0);
	float v3 = smoothNoise(x0,     y0 + 1);
	float v4 = smoothNoise(x0 + 1, y0 + 1);

	float i1 = interpolate(v1, v2, xd);
	float i2 = interpolate(v3, v4, xd);
	return     interpolate(i1, i2, yd);
}

float Perlin::smoothNoise(int x, int y) {
	float corners = (noise(x - 1, y - 1) + noise(x + 1, y - 1) + noise(x - 1, y + 1) + noise(x + 1, y + 1)) / 16.f;
	float sides   = (noise(x - 1, y)     + noise(x + 1, y)     + noise(x    , y - 1) + noise(x    , y + 1)) /  8.f;
	float center  = noise(x, y) / 4.f;
	return corners + sides + center;
}

float Perlin::noise(int x, int y) {
	int n = x + y * 57;
	n = (n << 13) ^ n;
	return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float Perlin::interpolate(float v1, float v2, float d) {
	//return v1 * (1 - d) + v2 * d;
	float ft = d * 3.1415927;
	float f = (1 - cos(ft)) * .5;

	return  v1 * (1 - f) + v2 * f;
}
